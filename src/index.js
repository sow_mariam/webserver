'use strict'

const my_shared_code_headless = require('./resource/js/my_shared_code_headless')


// serveur html
const express = require('express')
const app = express()

var bodyParser = require("body-parser")
app.use(bodyParser.urlencoded({ extended: true }))

//Déboguer les routes de votre application
const morgan = require('morgan')
app.use(morgan('dev'))



// …
let even_numbers = my_shared_code_headless.generateEvenNumbers(25)
console.log('even_numbers:', even_numbers)

app.use(express.static('public'))

app.get('/even_numbers', function (req, res) {
  res.send(even_numbers)
})

app.get('/info', function(request, response) {

  response.send('jsau-webserver-1.0.0');
})

app.get('/', function(request, response) {
  response.redirect(301,'html/test.html');
})

app.listen(3000, function () {
  console.log('le serveur écoute sur le port : 3000!')
})
