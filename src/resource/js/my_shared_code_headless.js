'use strict'

function generateEvenNumbers(max) {
// algorithme de calcul binaire
  var j=0;
  var t= [];//array();
  for(var i=0; i<=max; i++)
  {
      if(i%2==0)
      {
        
          t[j]=i;

          j=j+1;
      }
  }
    return t;
}

module.exports = {
    generateEvenNumbers
}
